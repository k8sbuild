profile=empty

#  --disable-textfield     disable enhanced text field widget
  --disable-fullscreen    disable XFree86 fullscreen detection
  --disable-gnomeui        enables GNOME UI support
  --enable-sdlui          enables SDL UI support
  --disable-gp2x           enables GP2X support
  --disable-wiz            enables WIZ support
  --disable-dingoo         enables native Dingoo support
  --disable-dingux         enables Dingux (Dingoo Linux) support
  --disable-nls           disables national language support
  --disable-realdevice    disables access to real peripheral devices (CBM4Linux/OpenCBM)
  --disable-ffmpeg        disable FFmpeg library support
  --disable-quicktime      enables Apple QuickTime support
  --disable-ethernet       enables The Final Ethernet emulation
  --disable-ipv6          disables the checking for IPv6 compatibility
#  --enable-parsid         enables ParSID support
  --disable-bundle        do not use application bundles on Macs
#  --enable-memmap         enable the memmap feature
#  --disable-editline      disable history in Cocoa UI's console
  --disable-lame          disable MP3 export with LAME
  --disable-static-lame    enable static LAME linking
  --disable-rs232         disable RS232 support
  --disable-midi          disable MIDI support
  --disable-embedded       enable embedding of emulation data files
  --disable-hidmgr        disable IOHIDManager joystick support on Mac
  --disable-hidutils      disable HID Uitlities joystick support on Mac
  --disable-debug          enable debug source options
  --disable-debug-code     enable debugging code
#  --enable-inline         enable inlining of functions default=yes
#  --enable-arch[=arch]    enable architecture specific compilation [default=yes]
#  --enable-sse            enable the use of SSE [default=yes]
#  --enable-no-pic         enable the use of the no-pic switch [default=yes]

  --without-xaw3d            use Xaw3d library instead of plain Xaw
#  --without-readline      do not try to use the system's readline library
  --without-midas            use MIDAS sound system instead of Allegro for audio
  --without-arts             use aRts sound system
  --without-pulse         do not use PulseAudio sound system
  --with-alsa          do not use the ALSA sound system
  --without-oss           do not use the OSS sound system
  --without-sdlsound         use SDL sound system
#  --without-resid         do not use the reSID engine
#  --without-png           do not use the PNG screenshot system
#  --without-zlib          do not use the zlib support
  --without-picasso96        use Amiga P96 grafix system instead of cgx
  --without-cocoa            enables native Cocoa UI on Macs
#  --with-uithreads        enables multi threading for Unix UIs
#  --with-x                use the X Window System


# giflib patch
[preconfig-patch]
--- vice-2.4/src/gfxoutputdrv/gifdrv.c.orig	2012-07-26 02:46:05.000000000 +0300
+++ vice-2.4/src/gfxoutputdrv/gifdrv.c	2015-12-08 12:16:54.000000000 +0200
@@ -50,6 +50,14 @@
 #define VICE_FreeMapObject FreeMapObject
 #endif
 
+static int closeGif (GifFileType* file) {
+#if ((GIFLIB_MAJOR == 5 && GIFLIB_MINOR >= 1) || GIFLIB_MAJOR > 5)
+  return EGifCloseFile(file, NULL);
+#else
+  return EGifCloseFile(file);
+#endif
+}
+
 typedef struct gfxoutputdrv_data_s
 {
   GifFileType *fd;
@@ -114,7 +122,7 @@ static int gifdrv_open(screenshot_t *scr
   if (EGifPutScreenDesc(sdata->fd, screenshot->width, screenshot->height, 8, 0, gif_colors) == GIF_ERROR ||
       EGifPutImageDesc(sdata->fd, 0, 0, screenshot->width, screenshot->height, 0, NULL) == GIF_ERROR)
   {
-    EGifCloseFile(sdata->fd);
+    closeGif(sdata->fd);
     VICE_FreeMapObject(gif_colors);
     lib_free(sdata->data);
     lib_free(sdata->ext_filename);
@@ -145,7 +153,7 @@ static int gifdrv_close(screenshot_t *sc
 
     sdata = screenshot->gfxoutputdrv_data;
 
-    EGifCloseFile(sdata->fd);
+    closeGif(sdata->fd);
     VICE_FreeMapObject(gif_colors);
 
     /* for some reason giflib will create a file with unexpected
@@ -184,7 +192,7 @@ static char *gifdrv_memmap_ext_filename;
 
 static int gifdrv_close_memmap(void)
 {
-  EGifCloseFile(gifdrv_memmap_fd);
+  closeGif(gifdrv_memmap_fd);
   VICE_FreeMapObject(gif_colors);
   lib_free(gifdrv_memmap_ext_filename);
 
@@ -231,7 +239,7 @@ static int gifdrv_open_memmap(const char
   if (EGifPutScreenDesc(gifdrv_memmap_fd, x_size, y_size, 8, 0, gif_colors) == GIF_ERROR ||
       EGifPutImageDesc(gifdrv_memmap_fd, 0, 0, x_size, y_size, 0, NULL) == GIF_ERROR)
   {
-    EGifCloseFile(gifdrv_memmap_fd);
+    closeGif(gifdrv_memmap_fd);
     VICE_FreeMapObject(gif_colors);
     lib_free(gifdrv_memmap_ext_filename);
     return -1;
[/preconfig-patch]


!DESC
VICE -- Versatile Commodore 8-bit Emulator

VICE, the multi-platform C64, C128, VIC20, PET, PLUS4 and CBM-II
emulator. This version can be compiled for MSDOS, Win32, RiscOS,
OS/2, BeOS, QNX 4.x, QNX 6.x, AmigaOS, GP2X, Dingoo, Syllable,
SkyOS and for most Unix systems provided with the X Window
System version 11, R5 or later.
!EOF
