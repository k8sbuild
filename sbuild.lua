#!/usr/bin/lua
-- semi-automated SlackBuild script

local pinfo;
local pkgdir;
local buildDirName;
local getPrefixDir;

local myOpt = {
  chownUser = "",
  --profile = "speed",
  profile = "empty",
  cfgname = "./configure",
  install_path = "/usr",
  pkgExt = "txz",
  srcBuildUser = "",
  pkgdestdir = "",
  no_pcp = false,
  no_prcp = false,
  no_dox = false,
  no_cfg = false,
  no_pss = false,
  make_build_dir = false,
  keep_build_dir = false,
  use_fakeroot = false,
  using_cmake = false,
  --force_native_profile = true,
  --no_docdir = true,
  --no_libdir = true,
  --no_localstatedir = true,
};


local function getCPUs ()
  local fi =assert(io.popen("nproc", "r"));
  local n = assert(fi:read("*n"));
  --if n > 2 then n = n-1; end; -- leave one core free for other tasks
  return n;
end;

local jobs = getCPUs();
local force_native_profile = true;



local function logMessage (fmt, ...)
  local s = fmt:format(...);
  s = s:gsub("\n", "\n*** ");
  io.write("*** ", s, "\n"); io.flush();
end;


local function logMessageToFile (fmt, ...)
  local s = fmt:format(...);
  s = s:gsub("\n", "\n*** ");
  local fo = io.open("/tmp/zsbuild_times.log", "a");
  fo:write("*** ", s, "\n");
  fo:close();
  io.write("*** ", s, "\n"); io.flush();
end;


local function timeStr (secs)
  local mins = math.floor(secs/60);
  secs = secs%60;
  local hours = math.floor(mins/60);
  mins = mins%60;
  return ("%02d:%02d:%02d"):format(hours, mins, secs);
end;


-- get user
--[[
local meUser, meGroup;
(function ()
  local fl = assert(io.popen("whoami", "r"));
  local s = assert(fl:read());
  fl:close();
  s = s:match("^%s*(.-)%s*$");
  if s == "" then error("who am i?!"); end;
  --print("me: "..s);
  meUser = s;
end)();
-- get group
(function ()
  local fl = assert(io.popen("id -gn", "r"));
  local s = assert(fl:read());
  fl:close();
  s = s:match("^%s*(.-)%s*$");
  if s == "" then error("who am i?!"); end;
  --print("me: "..s);
  meGroup = s;
end)();
myOpt.chownUser = meUser..":"..meGroup;
]]


local weAreBuildingSrcz = false; -- this flag means that we are building sources and shouldn't be 'root'


local pkgDestDir;

local optPathList = {
  "./!.xopt",
  "~/.sbuild/!.xopt",
  "~/.sbuild/xopt/!.xopt",
};


-- -mpreferred-stack-boundary=2

-- profiles
local defCompOptProfs = {
  speed = {
    CFLAGS = "-O3 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O3 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  speedfastmath = {
    CFLAGS = "-O3 -march=core2 -mtune=core2 -fwrapv -ffast-math -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O3 -march=core2 -mtune=core2 -fwrapv -ffast-math -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  native = {
    CFLAGS = "-O2 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  nativenoalias = {
    CFLAGS = "-O2 -march=core2 -mtune=core2 -fno-strict-aliasing -fwrapv -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=core2 -mtune=core2 -fno-strict-aliasing -fwrapv -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  nativenoaliasopt = {
    CFLAGS = "-O3 -march=core2 -mtune=core2 -fno-strict-aliasing -fwrapv -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O3 -march=core2 -mtune=core2 -fno-strict-aliasing -fwrapv -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  size = {
    CFLAGS = "-Os -march=i586 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-Os -march=i586 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  wmaker = {
    CFLAGS = "-O1 -fomit-frame-pointer -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O1 -fomit-frame-pointer -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  debug = {
    CFLAGS = "-O0 -g -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O0 -g -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    LDFLAGS = "-g"
  },
  empty = {
    CFLAGS = "",
    CXXFLAGS = "",
  },
  nativeo2 = {
    CFLAGS = "-O2 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=core2 -mtune=core2 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  nativeemacs = {
    CFLAGS = "-O2 -march=core2 -mtune=core2 -Wno-pointer-sign -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=core2 -mtune=core2 -Wno-pointer-sign -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  nativeo1 = {
    CFLAGS = "-O1 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O1 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  pngfix = {
    CFLAGS = "-Dpng_set_gray_1_2_4_to_8=png_set_expand_gray_1_2_4_to_8",
    CXXFLAGS = "-Dpng_set_gray_1_2_4_to_8=png_set_expand_gray_1_2_4_to_8",
  },
  original = {
    CFLAGS = "",
    CXXFLAGS = "",
  },
  i586 = {
    CFLAGS = "-O2 -march=i586 -mtune=i586 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=i586 -mtune=i586 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  i686 = {
    CFLAGS = "-O2 -march=i686 -mtune=i686 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=i686 -mtune=i686 -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  core2 = {
    CFLAGS = "-O2 -march=core2 -mtune=core2 -mno-avx -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=core2 -mtune=core2 -mno-avx -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  pentium4 = {
    CFLAGS = "-O2 -march=pentium4 -mtune=pentium4 -mno-avx -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=pentium4 -mtune=pentium4 -mno-avx -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  nehalem = {
    CFLAGS = "-O2 -march=nehalem -mtune=nehalem -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=nehalem -mtune=nehalem -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
  haswell = {
    CFLAGS = "-O2 -march=haswell -mtune=haswell -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
    CXXFLAGS = "-O2 -march=haswell -mtune=haswell -fwrapv -fno-strict-aliasing -fno-delete-null-pointer-checks -fno-strict-overflow -mstackrealign",
  },
};



local confOpt = {};
local compOpt = {};
local slackDesc = {};
local slackReq = {};
local unpackScript = {};
local postcfgPatches = {};
local precfgPatches = {};
local doclist = {}; -- [docs...]
local cfglist = {};
local psslist = {};
local doinstlist = {};
local scinstalllist = {};
local scmakelist = {};
local scconfigurelist = {};
local scpreconfigurelist = {};
local scpremakelist = {};
local buildEnvVars = {};


local join = table.concat;


local function verCompare (v0, v1)
  local s0, s1 = {}, {};
  print(v0 < v1);
  for m in v0:gmatch("[^.]+") do s0[#s0+1] = m; end;
  for m in v1:gmatch("[^.]+") do s1[#s1+1] = m; end;
  local f = 1;
  while f <= #s0 and f <= #s1 do
    local n0 = tonumber(s0[f]);
    local n1 = tonumber(s1[f]);
    if not n0 or not n1 then
      n0 = s0[f]:lower();
      n1 = s1[f]:lower();
    end;
    if n0 < n1 then return -1;
    elseif n0 > n1 then return 1;
    end;
    f = f+1;
  end;
  if #s0 < #s1 then return -1;
  elseif #s0 > #s1 then return 1;
  end;
  return 0;
end;


local function isShellCh (ch)
  if (ch >= "\1" and ch <= "\42") or
     (ch >= "\59" and ch <= "\60") or
     (ch >= "\62" and ch <= "\63") or
     (ch >= "\91" and ch <= "\93") or
     (ch >= "\123" and ch <= "\125") or ch == "\96" then
    return true;
  end;

  return false;
end;


function string.shQuote (s)
  s = s:gsub(".", function (ch)
    if ch == "\0" then return ""; end;
    if isShellCh(ch) then return "\\"..ch; end; --
    return ch;
  end);

  return s;
end;


-- find and open file
-- not pathList: take optPathList
-- return: io-file, filename or false
local function openFile (fname, pathList)
  pathList = pathList or optPathList;
  for _, path in ipairs(pathList) do
    local fn = path:gsub("!!", "\0");
    fn = fn:gsub("!", fname);
    fn = fn:gsub("%z", "!");
    if fn:match("^~/") then
      local hp = os.getenv("HOME");
      if not hp or hp == "" then error("no $HOME var set!"); end;
      if not hp:match("/$") then hp = hp.."/"; end;
      fn = hp..fn:sub(3, -1);
    end;
    local fl = io.open(fn, "r");
    if fl then
      io.stderr:write("using ", fn, "\n"); io.stderr:flush();
      return fl, fn;
    end;
  end;

  return false;
end;


-- i'm too lazy to do regexps
-- parse option string
local function parseOption (s)
  local res, qq, screen, iscfgopt = {};

  local o = s;
  s = s:match("^%s*(.-)%s*$");
  if s:match("^[#;]") then return false; end;
  if s:match("^%-%-") then
    iscfgopt = true;
    s = s:sub(3, -1);
    s = s:match("^%s*(.-)%s*$");
  else
    iscfgopt = false;
    if o:match("^%s") then return false; end; -- some silly text
  end;
  if s == "" then return false; end;
  for ch in s:gmatch(".") do
    if screen then
      screen = false;
    else
      if ch == "\92" then screen = true;
      elseif ch == '"' then qq = not qq;
      elseif not qq and ch <= " " then break;
      end;
    end;
    if not screen then res[#res+1] = ch; end;
  end;
  if #res < 1 then return false; end;
  if iscfgopt then res[1] = "--"..res[1]; end;

  return join(res), iscfgopt;
end;


-- if iscfgopt is false, treat as compiler option
local function parseCMakeOption (s)
  local iscfgopt = false;

  local o = s;
  s = s:match("^%s*(.-)%s*$");
  if s:match("^[#;]") then return false; end;
  if s == "" then return false; end;
  if s:match("^%-") then
    iscfgopt = true;
    s = s:match("^%s*(.-)%s*$");
    return s, iscfgopt;
  elseif o:match("^%s") then
    iscfgopt = true;
    s = s:match("^([^%s]+)");
    s = "-D"..s;
    return s, iscfgopt;
  end;

  local res, qq, screen = {};

  for ch in s:gmatch(".") do
    if screen then
      screen = false;
    else
      if ch == "\92" then screen = true;
      elseif ch == '"' then qq = not qq;
      elseif not qq and ch <= " " then break;
      end;
    end;
    if not screen then res[#res+1] = ch; end;
  end;

  return join(res), false;
end;


-- split to name/value pair
-- return: name, value, condition (char after "=" or false)
local function nameValueSplit (s)
  local n, c, v = s:match("^([^=]+)%=([?+!]?)(.*)$");
  if not n or not c or not v then return s:match("^%s*(.-)%s*$"), "", false; end;
  if c == "" then c = false; end;
  v = v:match("^%s*(.-)%s*$");
  local vv = v:match('^"(.-)"$'); if vv then v = vv; end;
  return n:match("^%s*(.-)%s*$"), v, c;
end;


local function setProfile (name)
  local prof = defCompOptProfs[name];
  assert(prof, "unknown compiler profile");
  for n, v in pairs(prof) do
    if not compOpt[n] then compOpt[n] = v; end;
    --print(n, v);
  end;
  myOpt.profile = name;
end;


local function fixFuckinCaret ()
  if compOpt["CFLAGS"] then
    compOpt["CFLAGS"] = compOpt["CFLAGS"].." -fno-diagnostics-show-caret";
  end;
  if compOpt["CXXFLAGS"] then
    compOpt["CXXFLAGS"] = compOpt["CXXFLAGS"].." -fno-diagnostics-show-caret";
  end;
end;


local function copyEnvVars ()
  for k, v in pairs(buildEnvVars) do
    if v == "" then
      compOpt[k] = nil;
    else
      compOpt[k] = v;
    end;
  end;
end;


local function optparseBooleanOptValue (optname, value)
  if not value then value = "ona"; end;
  value = value:lower();
  --
  if value == "tan" or value == "yes" or value == "true" or
     value == "1" or value == "t" or value == "y" then
    return true;
  end;
  --
  if value == "ona" or value == "no" or value == "false" or
     value == "0" or value == "o" or value == "n" or value == "f" then
    return false;
  end;
  --
  error("option '"..optname.."' expects boolean, but got '"..value.."'");
end;


local function optparseBooleanOpt (name, value, cchar, str)
  --print("name=["..name.."]; value=["..value.."]");
  myOpt[name] = optparseBooleanOptValue(name, value);
  --local v = myOpt[name];
  --if v then v = "tan"; else v = "ona"; end;
  --logMessage("BOOL: '%s' = '%s' (%s)", name, value or "", v);
end;


-- cchar: condition (char after "=" or false)
-- str: unparsed string
local myOptH = {
  chown_user = {
    dsc = "chown_user=username[:group] -- NO DEFAULT!",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      myOpt.chownUser = value;
    end;
  },
  pkg_dest_dir = {
    dsc = "pkg_dest_dir=path-for-package (~/.sbuild/pkg)",
    handler = function (name, value, cchar, str)
      if not value then error("wtf?!"); end;
      myOpt.pkgdestdir = value;
    end;
  },
  pkg_ext = {
    help = "pkg_ext=[txz]",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      myOpt.pkgExt = value;
    end;
  },
  src_build_user = {
    help = "src_build_user  user to chown when building the package (default to chown_user)",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      myOpt.srcBuildUser = value;
    end;
  },
  profile = {
    help = "profile=[native|speed|size|empty]",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      setProfile(value);
    end;
  },
  xflags = {
    help = "xflags=[+]flags -- modify CFLAGS and CXXFLAGS",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      if cchar == "+" then
        if value ~= "" then
          if compOpt.CFLAGS then compOpt.CFLAGS = compOpt.CFLAGS.." ";
          else compOpt.CFLAGS = "";
          end;
          compOpt.CFLAGS = compOpt.CFLAGS..value;
          --
          if compOpt.CXXFLAGS then compOpt.CXXFLAGS = compOpt.CXXFLAGS.." ";
          else compOpt.CXXFLAGS = "";
          end;
          compOpt.CXXFLAGS = compOpt.CXXFLAGS..value;
        end;
      elseif not cchar then
        compOpt.CFLAGS = value;
        compOpt.CXXFLAGS = value;
      else
        assert(false, "invalid cflags operation");
      end;
    end;
  },
  cflags = {
    help = "cflags=[+]flags -- modify CFLAGS",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      if cchar == "+" then
        if value ~= "" then
          if compOpt.CFLAGS then compOpt.CFLAGS = compOpt.CFLAGS.." ";
          else compOpt.CFLAGS = "";
          end;
          compOpt.CFLAGS = compOpt.CFLAGS..value;
        end;
      elseif not cchar then
        compOpt.CFLAGS = value;
      else
        assert(false, "invalid cflags operation");
      end;
    end;
  },
  cxxflags = {
    help = "cxxflags=[+]flags -- modify CXXFLAGS",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      if cchar == "+" then
        if value ~= "" then
          if compOpt.CXXFLAGS then compOpt.CXXFLAGS = compOpt.CXXFLAGS.." ";
          else compOpt.CXXFLAGS = "";
          end;
          compOpt.CXXFLAGS = compOpt.CXXFLAGS..value;
        end;
      elseif not cchar then
        compOpt.CXXFLAGS = value;
      else
        assert(false, "invalid cflags operation");
      end;
    end;
  },
  cfg_name = {
    help = "cfg_name=configure_script_name",
    handler = function (name, value, cchar, str)
      if not value then value = "./configure"; end;
      myOpt.cfgname = value;
    end;
  },
  makename = {
    help = "makename=make_command",
    handler = function (name, value, cchar, str)
      if not value then value = "make"; end;
      myOpt.makename = value;
    end;
  },
  instname = {
    help = "instname=install_command",
    handler = function (name, value, cchar, str)
      if not value then value = "make"; end;
      myOpt.instname = value;
    end;
  },
  no_prefix = {
    help = "no_prefix=[tan]",
    handler = optparseBooleanOpt,
  },
  no_mandir = {
    help = "no_mandir=[tan]",
    handler = optparseBooleanOpt,
  },
  no_docdir = {
    help = "no_docdir=[tan]",
    handler = optparseBooleanOpt,
  },
  no_libdir = {
    help = "no_libdir=[tan]",
    handler = optparseBooleanOpt,
  },
  no_localstatedir = {
    help = "no_localstatedir=[tan]",
    handler = optparseBooleanOpt,
  },
  no_sysconfdir = {
    help = "no_sysconfdir=[tan]",
    handler = optparseBooleanOpt,
  },
  localstatedir = {
    help = "localstatedir=path",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      myOpt.localstatedir = value;
    end;
  },
  no_programprefix = {
    help = "no_programprefix=[tan]",
    handler = optparseBooleanOpt,
  },
  no_programsuffix = {
    help = "no_programsuffix=[tan]",
    handler = optparseBooleanOpt,
  },
  no_buildarch = {
    help = "no_buildarch=[tan]",
    handler = optparseBooleanOpt,
  },
  no_strip = {
    help = "no_strip=[tan]",
    handler = optparseBooleanOpt,
  },
  no_preconfigure = {
    help = "no_preconfigure=[tan]",
    handler = optparseBooleanOpt,
  },
  no_premake = {
    help = "no_premake=[tan]",
    handler = optparseBooleanOpt,
  },
  no_configure = {
    help = "no_configure=[tan]",
    handler = optparseBooleanOpt,
  },
  no_make = {
    help = "no_make=[tan]",
    handler = optparseBooleanOpt,
  },
  no_make_argflags = {
    help = "no_make_argflags=[tan] -- do not pass vars as make arguments",
    handler = optparseBooleanOpt,
  },
  no_inst_destdir = {
    help = "no_inst_destdir=[tan] -- do not pass DESTDIR to installer",
    handler = optparseBooleanOpt,
  },
  no_package = {
    help = "no_package=[tan]",
    handler = optparseBooleanOpt,
  },
  no_deptrack = {
    help = "no_deptrack=[tan]",
    handler = optparseBooleanOpt,
  },
  no_pcp = {
    help = "no_pcp=[tan] -- disable postconfig patches",
    handler = optparseBooleanOpt,
  },
  no_prcp = {
    help = "no_prcp=[tan] -- disable preconfig patches",
    handler = optparseBooleanOpt,
  },
  no_dox = {
    help = "no_dox=[tan] -- disable doc copying",
    handler = optparseBooleanOpt,
  },
  no_cfg = {
    help = "no_cfcopy=[tan] -- disable config copying",
    handler = optparseBooleanOpt,
  },
  no_pss = {
    help = "no_psscripts=[tan] -- disable post-install scripts",
    handler = optparseBooleanOpt,
  },
  make_install_var = {
    help = "make_install_var=var_name_for_make_install_root",
    handler = function (name, value, cchar, str)
      value = value:match("^%s*(.-)%s*$");
      myOpt.make_install_var = value;
      if value == "" then myOpt.make_install_var = "THIS_IS_A_USELESS_TEMP_VAR"; end;
    end;
  },
  pkg_author = {
    help = "pkg_author=k8",
    handler = function (name, value, cchar, str)
      if not value then value = ""; end;
      value = value:match("^%s*(.-)%s*$");
      myOpt.pkg_author = value;
    end;
  },
  install_path = {
    help = "install_path=[/usr]",
    handler = function (name, value, cchar, str)
      if not value then value = "/usr"; end;
      value = value:match("^%s*(.-)%s*$");
      myOpt.install_path = value;
    end;
  },
  make_build_dir = {
    help = "make_build_dir=[tan] -- for cmake, for example",
    handler = optparseBooleanOpt,
  },
  keep_build_dir = {
    help = "keep_build_dir=[tan] -- do not remove building directory",
    handler = optparseBooleanOpt,
  },
  use_fakeroot = {
    help = "use_fakeroot=[tan] -- use fakeroot instead of su/sudo",
    handler = optparseBooleanOpt,
  },
  using_cmake = {
    help = "using_cmake=[tan] -- cmake is used as build system",
    handler = optparseBooleanOpt,
  },
  --force_native_profile = {
  --  help = "force_native_profile=[ona] -- replace 'empty' profile with 'native'",
  --  handler = optparseBooleanOpt,
  --},
};


local function compOptParse (s)
  local n, v, c = nameValueSplit(s);
  local opt = myOptH[n];
  if opt and opt.handler then opt.handler(n, v, c, s);
  else
    if not n:match("^[A-Z_]+$") then error("unknown option: "..n); end;
    -- compiler option
    local opt = compOpt[n];
    if c == "?" then -- set if not set yet
      if not opt then compOpt[n] = v; end;
    elseif c == "+" then -- add
      opt = opt and (opt.." ") or "";
      compOpt[n] = opt..v;
    elseif c == "!" then -- set if already set
      if opt then compOpt[n] = v; end;
    else compOpt[n] = v;
    end;
  end;
end;


local function loadOptions (fname, failOnError)
  local phase = false;
  local tdsc = fname..":";
  local tdsc1 = tdsc.." ";
  local dsccnt = 0;

  function phaseNothing (s)
    local opt, iscfg;
    -- ^var=name -- env var
    if s ~= "" and s:sub(1, 1) == "^" then
      local k, v = s:match("%^%s*([^=]-)%s*=%s*(.-)%s*$");
      if k == "" then error("invalid envvar declaration: "..s); end;
      --if v ~= "" then buildEnvVars[k] = v; else buildEnvVars[k] = nil; end;
      buildEnvVars[k] = v;
      --if v ~= "" then compOpt[k] = v; else compOpt[k] = nil; end;
      --print("k="..k.."; v="..v.."|");
      return;
    end;
    if myOpt.using_cmake then
      opt, iscfg = parseCMakeOption(s);
      --print(opt, ":", iscfg);
    else
      opt, iscfg = parseOption(s);
    end;
    if opt then
      if iscfg then
        -- configure option
        confOpt[#confOpt+1] = opt;
      else
        compOptParse(s);
      end;
    end;
  end;

  function phaseDescr (s)
    if s ~= "" or dsccnt > 0 then
      if #slackDesc == 0 then
        slackDesc[#slackDesc+1] = "# HOW TO EDIT THIS FILE:";
        slackDesc[#slackDesc+1] = '# The "handy ruler" below makes it easier to edit a package description.  Line';
        slackDesc[#slackDesc+1] = "# up the first '|' above the ':' following the base package name, and the '|'";
        slackDesc[#slackDesc+1] = "# on the right side marks the last column you can put a character in.  You must";
        slackDesc[#slackDesc+1] = "# make exactly 11 lines for the formatting to be correct.  It's also";
        slackDesc[#slackDesc+1] = "# customary to leave one space after the ':'.";
        slackDesc[#slackDesc+1] = "";
        local ss = string.rep(" ", #fname).."|-----handy-ruler------------------------------------------------------|";
        slackDesc[#slackDesc+1] = ss;
      end;
      s = s:match("^%s*(.-)%s*$");
      if s:sub(1, 1) ~= "#" then
        if #s > 70 then error("package description: line too long (must be <= 70 chars)\n"..s); end;
        dsccnt = dsccnt+1;
        if dsccnt == 12 then error("package description: too many lines (11 max)\n"); end;
        if s == "" then s = tdsc; else s = tdsc1..s; end;
        slackDesc[#slackDesc+1] = s;
      end;
    end;
    return false;
  end;

  function phaseReq (s)
    s = s:match("^%s*(.-)%s*$");
    local rc = #slackReq;
    if s:match("^[^#]") then
      if s:lower() == "or" or s == "|" then
        if rc >= 1 then slackReq[#slackReq+1] = " | "; end;
      else
        if rc >= 1 and slackReq[rc] ~= " | " then slackReq[#slackReq+1] = ","; end;
      end;
      slackReq[#slackReq+1] = s;
    end;
  end;

  function phaseUnp (s)
    unpackScript[#unpackScript+1] = s;
  end;


  -----------------------------------------------------------------------------
  local pcpStarted = false;
  local pcpPre = false;
  local pcpCurrent = false;

  function phasePCPNew (s)
    if pcpStarted then error("unclosed postconfig-patch section in file "..fname); end;
    pcpStarted = true;
    pcpPre = false;
    pcpCurrent = {};
  end;

  function phasePRCPNew (s)
    if pcpStarted then error("unclosed preconfig-patch section in file "..fname); end;
    pcpStarted = true;
    pcpPre = true;
    pcpCurrent = {};
  end;

  function phasePCPEnd (s)
    if not pcpStarted then error("bad postconfig-patch section in file "..fname); end;
    if pcpPre then
      precfgPatches[#precfgPatches+1] = pcpCurrent;
    else
      postcfgPatches[#postcfgPatches+1] = pcpCurrent;
    end;
    pcpStarted = false;
    pcpCurrent = false;
    pcpPre = false;
  end;

  function phasePCP (s, origs)
    pcpCurrent[#pcpCurrent+1] = origs;
  end;


  -----------------------------------------------------------------------------
  local doxStarted = false;
  local doxCurrent = false;
  local doxPath = false;

  function phaseDoxNew (s)
    if doxStarted then error("unclosed dox section in file "..fname); end;
    doxStarted = true;
    doxCurrent = {};
    if not doxPath then doxPath = ""; end;
  end;

  function phaseDoxEnd (s)
    if not doxStarted then error("bad dox section in file "..fname); end;
    doclist[#doclist+1] = {["path"] = doxPath, ["list"] = doxCurrent};
    doxStarted = false;
    doxCurrent = false;
    doxPath = false;
  end;

  function phaseDox (s, origs)
    doxCurrent[#doxCurrent+1] = origs;
  end;


  -----------------------------------------------------------------------------
  local cfgStarted = false;

  function phaseCfgNew (s)
    if cfgStarted then error("unclosed cfg section in file "..fname); end;
    cfgStarted = true;
  end;

  function phaseCfgEnd (s)
    if not cfgStarted then error("bad cfg section in file "..fname); end;
    cfgStarted = false;
  end;

  function phaseCfg (s, origs)
    cfglist[#cfglist+1] = origs;
  end;


  -----------------------------------------------------------------------------
  local pssStarted = false;
  local pssCurrent = false;

  function phasePSSNew (s)
    if pssStarted then error("unclosed postinstall section in file "..fname); end;
    pssStarted = true;
    pssCurrent = {};
  end;

  function phasePSSEnd (s)
    if not pssStarted then error("bad postinstall section in file "..fname); end;
    --
    while #pssCurrent > 0 and pssCurrent[#pssCurrent] == "" do table.remove(pssCurrent); end;
    --
    if #pssCurrent > 0 then
      pssCurrent[#pssCurrent+1] = "";
      psslist[#psslist+1] = table.concat(pssCurrent, "\n");
    end;
    --
    pssStarted = false;
    pssCurrent = false;
  end;

  function phasePSS (s, origs)
    pssCurrent[#pssCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local doinstStarted = false;
  local doinstCurrent = false;

  function phaseDoInstNew (s)
    if doinstStarted then error("unclosed doinst.sh section in file "..fname); end;
    doinstStarted = true;
    doinstCurrent = {};
  end;

  function phaseDoInstEnd (s)
    if not doinstStarted then error("bad doinst.sh section in file "..fname); end;
    --
    while #doinstCurrent > 0 and doinstCurrent[#doinstCurrent] == "" do table.remove(doinstCurrent); end;
    --
    if #doinstCurrent > 0 then
      doinstCurrent[#doinstCurrent+1] = "";
      if #doinstlist > 0 then doinstlist[#doinstlist+1] = "\n"; end;
      doinstlist[#doinstlist+1] = table.concat(doinstCurrent, "\n");
    end;
    --
    doinstStarted = false;
    doinstCurrent = false;
  end;

  function phaseDoInst (s, origs)
    doinstCurrent[#doinstCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local scinstallStarted = false;
  local scinstallCurrent = false;

  function phaseInstallNew (s)
    if scinstallStarted then error("unclosed install section in file "..fname); end;
    scinstallStarted = true;
    scinstallCurrent = {};
  end;

  function phaseInstallEnd (s)
    if not scinstallStarted then error("bad install section in file "..fname); end;
    --
    while #scinstallCurrent > 0 and scinstallCurrent[#scinstallCurrent] == "" do table.remove(scinstallCurrent); end;
    --
    if #scinstallCurrent > 0 then
      scinstallCurrent[#scinstallCurrent+1] = "";
      scinstalllist[#scinstalllist+1] = table.concat(scinstallCurrent, "\n");
    end;
    --
    scinstallStarted = false;
    scinstallCurrent = false;
  end;

  function phaseInstall (s, origs)
    scinstallCurrent[#scinstallCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local scmakeStarted = false;
  local scmakeCurrent = false;

  function phaseMakeNew (s)
    if scmakeStarted then error("unclosed make section in file "..fname); end;
    scmakeStarted = true;
    scmakeCurrent = {};
  end;

  function phaseMakeEnd (s)
    if not scmakeStarted then error("bad make section in file "..fname); end;
    --
    while #scmakeCurrent > 0 and scmakeCurrent[#scmakeCurrent] == "" do table.remove(scmakeCurrent); end;
    --
    if #scmakeCurrent > 0 then
      scmakeCurrent[#scmakeCurrent+1] = "";
      scmakelist[#scmakelist+1] = table.concat(scmakeCurrent, "\n");
    end;
    --
    scmakeStarted = false;
    scmakeCurrent = false;
  end;

  function phaseMake (s, origs)
    scmakeCurrent[#scmakeCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local scconfigureStarted = false;
  local scconfigureCurrent = false;

  function phaseConfigureNew (s)
    if scconfigureStarted then error("unclosed configure section in file "..fname); end;
    scconfigureStarted = true;
    scconfigureCurrent = {};
  end;

  function phaseConfigureEnd (s)
    if not scconfigureStarted then error("bad configure section in file "..fname); end;
    --
    while #scconfigureCurrent > 0 and scconfigureCurrent[#scconfigureCurrent] == "" do table.remove(scconfigureCurrent); end;
    --
    if #scconfigureCurrent > 0 then
      scconfigureCurrent[#scconfigureCurrent+1] = "";
      scconfigurelist[#scconfigurelist+1] = table.concat(scconfigureCurrent, "\n");
    end;
    --
    scconfigureStarted = false;
    scconfigureCurrent = false;
  end;

  function phaseConfigure (s, origs)
    scconfigureCurrent[#scconfigureCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local scpreconfigureStarted = false;
  local scpreconfigureCurrent = false;

  function phasePreConfigureNew (s)
    if scpreconfigureStarted then error("unclosed preconfigure section in file "..fname); end;
    scpreconfigureStarted = true;
    scpreconfigureCurrent = {};
  end;

  function phasePreConfigureEnd (s)
    if not scpreconfigureStarted then error("bad preconfigure section in file "..fname); end;
    --
    while #scpreconfigureCurrent > 0 and scpreconfigureCurrent[#scpreconfigureCurrent] == "" do table.remove(scpreconfigureCurrent); end;
    --
    if #scpreconfigureCurrent > 0 then
      scpreconfigureCurrent[#scpreconfigureCurrent+1] = "";
      scpreconfigurelist[#scpreconfigurelist+1] = table.concat(scpreconfigureCurrent, "\n");
    end;
    --
    scpreconfigureStarted = false;
    scpreconfigureCurrent = false;
  end;

  function phasePreConfigure (s, origs)
    scpreconfigureCurrent[#scpreconfigureCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local scpremakeStarted = false;
  local scpremakeCurrent = false;

  function phasePreMakeNew (s)
    if scpremakeStarted then error("unclosed premake section in file "..fname); end;
    scpremakeStarted = true;
    scpremakeCurrent = {};
  end;

  function phasePreMakeEnd (s)
    if not scpremakeStarted then error("bad premake section in file "..fname); end;
    --
    while #scpremakeCurrent > 0 and scpremakeCurrent[#scpremakeCurrent] == "" do table.remove(scpremakeCurrent); end;
    --
    if #scpremakeCurrent > 0 then
      scpremakeCurrent[#scpremakeCurrent+1] = "";
      scpremakelist[#scpremakelist+1] = table.concat(scpremakeCurrent, "\n");
    end;
    --
    scpremakeStarted = false;
    scpremakeCurrent = false;
  end;

  function phasePreMake (s, origs)
    scpremakeCurrent[#scpremakeCurrent+1] = origs:match("^.-%s*$");
  end;


  -----------------------------------------------------------------------------
  local phaseFunc = phaseNothing;

  local phaseAliases = {
    ["!DESCRIPTION"] = "!DSC",
    ["!DESCR"] = "!DSC",
    ["!DESC"] = "!DSC",
    ["!REQUIRED"] = "!REQ",
    ["!UNPACK"] = "!UNP",
  };

  local phases = {
    ["!DSC"] = { seen = false, func = phaseDescr },
    ["!REQ"] = { seen = false, func = phaseReq },
    ["!UNP"] = { seen = false, func = phaseUnp },
    ["[POSTCONFIG-PATCH]"] = { seen = false, allowMany = true, func = phasePCP, funcInit = phasePCPNew },
    ["[/POSTCONFIG-PATCH]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phasePCPEnd },
    ["[PRECONFIG-PATCH]"] = { seen = false, allowMany = true, func = phasePCP, funcInit = phasePRCPNew },
    ["[/PRECONFIG-PATCH]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phasePCPEnd },
    ["[DOCS]"] = { seen = false, allowMany = true, func = phaseDox, funcInit = phaseDoxNew },
    ["[/DOCS]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseDoxEnd },
    ["[DOX]"] = { seen = false, allowMany = true, func = phaseDox, funcInit = phaseDoxNew },
    ["[/DOX]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseDoxEnd },
    ["[CFG]"] = { seen = false, allowMany = true, func = phaseCfg, funcInit = phaseCfgNew },
    ["[/CFG]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseCfgEnd },
    ["[PRECONFIGURE]"] = { seen = false, allowMany = true, func = phasePreConfigure, funcInit = phasePreConfigureNew },
    ["[/PRECONFIGURE]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phasePreConfigureEnd },
    ["[PREMAKE]"] = { seen = false, allowMany = true, func = phasePreMake, funcInit = phasePreMakeNew },
    ["[/PREMAKE]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phasePreMakeEnd },
    ["[CONFIGURE]"] = { seen = false, allowMany = true, func = phaseConfigure, funcInit = phaseConfigureNew },
    ["[/CONFIGURE]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseConfigureEnd },
    ["[MAKE]"] = { seen = false, allowMany = true, func = phaseMake, funcInit = phaseMakeNew },
    ["[/MAKE]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseMakeEnd },
    ["[INSTALL]"] = { seen = false, allowMany = true, func = phaseInstall, funcInit = phaseInstallNew },
    ["[/INSTALL]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseInstallEnd },
    ["[POSTINSTALL]"] = { seen = false, allowMany = true, func = phasePSS, funcInit = phasePSSNew },
    ["[/POSTINSTALL]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phasePSSEnd },
    ["[DOINST.SH]"] = { seen = false, allowMany = true, func = phaseDoInst, funcInit = phaseDoInstNew },
    ["[/DOINST.SH]"] = { seen = false, allowMany = true, func = phaseNothing, funcInit = phaseDoInstEnd },
  };

  local files = {};
  local fileNames = {};


  function doInclude (fname, doFail)
    print("loading "..fname);
    local fl = openFile(fname);
    if not fl then
      if doFail or failOnError then error("can't open options file: "..fname); end;
      return false;
    end;
    files[#files+1] = fl;
    fileNames[#fileNames+1] = fname;
    return true;
  end;

  if not doInclude(fname, false) then return false; end;

  while #files > 0 do
    local fl = files[#files];
    local fname = fileNames[#fileNames];
    local s = fl:read();
    if s then
      local ostr = s;
      s = s:match("^(.-)%s*$");
      if s == "!EOF" then break; end;
      if s ~= "" and (s:sub(1, 1) == "!" or s:sub(1, 1) == "[") then
        local sn = phaseAliases[s];
        if not sn then sn = phaseAliases[s:upper()]; end;
        if sn then s = sn; end;
        --
        if s:match("^%s*!INCLUDE%s") then
          local fn = s:match("^.-!.-%s+(.-)$");
          if not fn or fn == "" then error("invalid include in "..fname); end;
          doInclude(fn, true);
        else
          if not doxPath and s:upper():match("^%[DOCS=") then
            local pth = s:match("^%[[Dd][Oo][Cc][Ss]=(.-)%]$");
            if pth then
              doxPath = pth;
              s = "[DOCS]";
            end;
          end;
          --
          local pp = phases[s];
          if not pp then pp = phases[s:upper()]; end;
          if not pp then error("unknown directive in "..fname..": "..s); end;
          if pp.seen and not pp.allowMany then error("duplicate directive in "..fname..": "..s); end;
          if pp.funcInit then pp.funcInit(s); end;
          phaseFunc = pp.func;
          pp.seen = true;
        end;
      else
        phaseFunc(s, ostr);
      end;
    else
      fl:close();
      table.remove(files);
      table.remove(fileNames);
    end;
  end;

  if dsccnt > 0 then
    while dsccnt < 11 do
      slackDesc[#slackDesc+1] = tdsc;
      dsccnt = dsccnt+1;
    end;
  end;

  return true;
end;


local function dumpOptions ()
  print("compiler options:");
  for k, v in pairs(compOpt) do
    print(string.format(" %s={%s}", k, tostring(v)));
  end;

  print("configure options:");
  for k, v in pairs(confOpt) do
    print(string.format(" %s={%s}", k, tostring(v)));
  end;

  print("other options:");
  for k, v in pairs(myOpt) do
    print(string.format(" %s={%s}", k, tostring(v)));
  end;
end;


--[[
if #arg < 1 then
  io.stderr:write("usage: dosb pkgname [build]\n");
  io.stderr:flush();
  os.exit(1);
end;
]]


local olderr = error;
function error (msg)
  if myOpt["chownUser"] ~= "" then
    logMessage("restoring owner: %s", myOpt["chownUser"]);
    os.execute("chown "..(myOpt["chownUser"]):shQuote().." -R .");
  end;
  olderr(msg);
end;


local function writeSlackDesc (path)
  if #slackDesc > 0 then
    os.execute("mkdir -p "..path:shQuote());
    local fl = io.open(path.."slack-desc", "w");
    if not fl then error("can't create 'slack-desc'"); end;
    for _, s in ipairs(slackDesc) do fl:write(s, "\n"); end;
    fl:close();
    return;
  end;
end;


local function writeSlackReq (path)
  if #slackReq > 0 then
    os.execute("mkdir -p "..path:shQuote());
    local fl = io.open(path.."slack-required", "w");
    if not fl then error("can't create 'slack-required'"); end;
    local s = table.concat(slackReq);
    fl:write(s, "\n");
    fl:close();
    return;
  end;
end;


local function getUID ()
  local fl = io.popen("echo $UID");
  if not fl then return -1; end;
  local s = fl:read();
  fl:close();

  return tonumber(s) or -1;
end;


local function getPrefixDir ()
  local ipath = myOpt.install_path or "/usr";
  ipath = ipath:match("^%s*(.-)%s*$");
  if ipath:match("/$") then ipath = ipath:sub(1, -2); end;
  if ipath == "" then ipath = "/usr"; end;
  return ipath;
end;


local build_stage = "configure"; -- "configure", "make"

local function getBuildFlags (nowrexport)
  local s = "";
  --logMessage("*STAGE*: %s (%s)", build_stage, (nowrexport and "nowrexport" or ""));
  if build_stage == "configure" then
    for k, v in pairs(compOpt) do
     --logMessage("  k=[%s] v=[%s]", k, v);
      if v and v ~= "" then
        if not nowrexport then
          s = s.."export "..k.."="..v:shQuote().." ; ";
        else
          s = s.." "..k.."="..v:shQuote();
        end;
      end;
    end;
  end;
  --logMessage("*S*: %s", s);
  return s;
end;


-- $PKG: package dir (/tmp/...)
-- $INSTPFX: install prefix (/usr)
-- $JOBS: number of jobs (1)
-- $PKGNAM: package name (fluxbox)
-- $VERSION: package version (1.4.1)
-- $BUILD: package build (1k8)
-- $ARCH: package architecture (i686)
-- $PKGDOCSDIR: docs directory (/tmp/...)
-- $USRDOCSDIR: install docs directory (/usr/doc/...)
-- pwd: original package directory (that one with 'configure')
local function getScriptVars (noexport)
  local usrdocpath = getPrefixDir().."/doc/"..pinfo.name.."-"..pinfo.version;
  local docpath = pkgdir..usrdocpath;
  --
  local vars = {
    PKG = pkgdir,
    INSTPFX = getPrefixDir(),
    PKGNAM = pinfo.name,
    VERSION = pinfo.version,
    ARCH = pinfo.arch,
    BUILD = tostring(pinfo.build)..pinfo.author,
    PKGDOCSDIR = docpath,
    USRDOCSDIR = usrdocpath,
    JOBS = tostring(jobs),
  };
  --
  local s = "";
  for n, v in pairs(vars) do
    --if not noexport then s = s.." export"; end;
    s = s.." "..n.."="..v:shQuote();
  end;
  for n, v in pairs(buildEnvVars) do
    --if not noexport then s = s.." export"; end;
    s = s.." "..n.."="..v:shQuote();
  end;
  --logMessage("*scv: %s", s);
  --error("DBGBREAK");
  return s;
end;


local function runCmdEx (loPriv, ignoreFail, cmd, ...)
  local s = cmd:format(...);
  logMessage("run: %s", s);
  --local flg = 'export CFLAGS="'..CFLAGS..'" ; export CXXFLAGS="'..CXXFLAGS..'" ; export LDFLAGS="'..LDFLAGS..'" ; ';
  local flg = "( ";
  if myOpt.make_build_dir then
    flg = flg.."cd "..buildDirName.." ; ";
  end;
  flg = flg..getBuildFlags()..s.." )";
  logMessage("*run: %s", flg);
  --print(flg);
  --s = s:gsub("\n", "\\\n");
  local tsh = false;
  if loPriv and not myOpt.use_fakeroot then
    if not myOpt.srcBuildUser or myOpt.srcBuildUser == "" then
      error("can't lower privileges for command!");
    end;
    --!logMessage("*LOPRV");
    --flg = "su "..myOpt.srcBuildUser..' -c "'..flg:shQuote()..'"';
    --logMessage("[%s]", flg);
    tsh = "/tmp/"..tostring(os.time())..".sh";
    local fl = io.open(tsh, "w+");
    if not fl then error("can't create file: "..tsh); end;
    --fl:write("whoami\n");
    fl:write(flg, "\n");
    fl:close();
    os.execute("chown "..myOpt.srcBuildUser.." "..tsh:shQuote());
    flg = "su "..myOpt.srcBuildUser..' -c "sh '..tsh:shQuote()..'"';
  end;
  local sttime = os.time();
  local res = os.execute(flg);
  local entime = os.time();
  if tsh then os.remove(tsh); end;
  logMessageToFile("%s [%s]", timeStr(entime-sttime), s);
  --os.exit(1);
  --local res = 0;
  if not ignoreFail and res ~= 0 then error("command is fucked up!"); end;
end;

local function runCmdRoot (cmd, ...)
  return runCmdEx(false, false, cmd, ...);
end;

local function runCmdRootFuckFail (cmd, ...)
  return runCmdEx(false, true, cmd, ...);
end;

local function runCmd (cmd, ...)
  return runCmdEx(true, false, cmd, ...);
end;

local function runCmdFuckFail (cmd, ...)
  return runCmdEx(true, true, cmd, ...);
end;


local function runShCmdEx (loPriv, cmd, nofail)
  logMessage("run:\n%s\n=====", cmd);
  local tsh = "/tmp/"..tostring(os.time())..".sh";
  local fl = io.open(tsh, "w+");
  if not fl then error("can't create file: "..tsh); end;
  fl:write(cmd, "\n");
  fl:close();
  --local flg = '( export CFLAGS="'..CFLAGS..'" ; export CXXFLAGS="'..CXXFLAGS..'" ; export LDFLAGS="'..LDFLAGS..'" ; bash '..tsh..' )';
  local flg = "( "..getBuildFlags().." "..getScriptVars().." bash "..tsh.." )";
  --s = s:gsub("\n", "\\\n");
  if loPriv then
    if not myOpt.srcBuildUser or myOpt.srcBuildUser == "" then
      error("can't lower privileges for command!");
    end;
    os.execute("chown "..myOpt.srcBuildUser.." "..tsh:shQuote());
    --!logMessage("*LOPRV");
    flg = "su "..myOpt.srcBuildUser..' -c "'..flg:shQuote()..'"';
  end;
  local res = os.execute(flg);
  --local res = 0;
  os.remove(tsh);
  if not nofail and res ~= 0 then error("command is fucked up!"); end;
end;

local function runShCmd (cmd, nofail)
  return runShCmdEx(true, cmd, nofail);
end;

local function runShCmdRoot (cmd, nofail)
  return runShCmdEx(false, cmd, nofail);
end;


local function runXCowSay (cmd, msg)
  local fl = io.popen("which xcowsay", "r");
  if fl then
    local s = fl:read();
    fl:close();
    if s and s ~= "" then
      runCmd(s, msg);
    end;
  end;
end;

local function parsePkgName (name)
  local spl = {};
  for w in (name.."-"):gmatch("[^-]*") do
    w = w:match("^%s*(.-)%s*$");
    if w ~= "" then spl[#spl+1] = w; end;
  end;
  assert(#spl >= 2);
  local pinfo = {};
  pinfo.version = spl[#spl];
  table.remove(spl);
  pinfo.name = join(spl, "-");
  pinfo.build = 1;
  pinfo.author = "";
  pinfo.arch = "i686";
  return pinfo;
end;


local function pkgInfoToStr (pinfo)
  local s = string.format("%s-%s-%s-%i%s", pinfo.name, pinfo.version, pinfo.arch, pinfo.build or 1, pinfo.author or "k8");
  return s;
end;


local function printHelp ()
  print([[
usage: sbuild pkgname-ver [options] [buildnum]
options:
  -nocfg     don't run configure
  -noprecfg  don't run preconfigure script
  -nopremk   don't run premake script
  -dump      dump vars
  -dummy     don't run anything
  -nomake    don't run make
  -nopkg     don't build package
  -nopcp     disable postconfig patches
  -noprcp    disable preconfig patches
  -jn        # of jobs (default: ]]..jobs..[[)
  -kbd       keep build dir
  -nonative  disable replacing 'empty' profile with 'native'

xopt options:]]); --
  local maxlen = 0;
  local names = {};
  for name, opt in pairs(myOptH) do
    if opt.help then
      names[#names+1] = name;
      maxlen = math.max(maxlen, #name);
    end;
  end;
  table.sort(names);
  --
  maxlen = maxlen+2;
  --
  for _, name in pairs(names) do
    local opt = myOptH[name];
    if opt.help then
      local lpad = string.rep(" ", maxlen-#name);
      print(name..lpad..opt.help);
    end;
  end;
end;


local apkgname = false;
local doDump = false;
local dummy = false;
local ooNoCfg = false;
local ooNoPreCfg = false;
local ooNoPreMk = false;
local ooNoMake = false;
local ooNoPkg = false;
local ooNoPCP = false;
local ooNoPRCP = false;
local bldnum = false;
local ooKBD = nil;
local inFakeRoot = false;

-- check for special flag
--[[
if arg[1] and arg[1] == "K8XBUILDSRC" then
  weAreBuildingSrcz = true;
  local cnt = #arg+1;
  for f = 2, cnt do arg[f-1] = arg[f]; end;
end;
]]

local in_pfx_arg = false;
local inst_pfx = false;


for f = 1, #arg do
  if in_pfx_arg then in_pfx_arg = false; inst_pfx = arg[f];
  elseif arg[f] == "-nocfg" or arg[f] == "--nocfg" then ooNoCfg = true;
  elseif arg[f] == "-noprecfg" or arg[f] == "--noprecfg" then ooNoPreCfg = true;
  elseif arg[f] == "-nopremk" or arg[f] == "--nopremk" then ooNoPreMk = true;
  elseif arg[f] == "-nomake" or arg[f] == "--nomake" then ooNoMake = true;
  elseif arg[f] == "-nopkg" or arg[f] == "--nopkg" then ooNoPkg = true;
  elseif arg[f] == "-nopcp" or arg[f] == "--nopcp" then ooNoPCP = true;
  elseif arg[f] == "-noprcp" or arg[f] == "--noprcp" then ooNoPCP = true;
  elseif arg[f] == "-dump" or arg[f] == "--dump" then doDump = true;
  elseif arg[f] == "-dummy" or arg[f] == "--dummy" then dummy = true;
  elseif arg[f] == "-kbd" or arg[f] == "--kbd" then ooKBD = true;
  elseif arg[f] == "-nonative" or arg[f] == "--nonative" then force_native_profile = false;
  elseif arg[f] == "-pfx" or arg[f] == "--pfx" or
         arg[f] == "-prefix" or arg[f] == "--prefix" then
    in_pfx_arg = true;
    if f+1 > #arg then
      io.stderr:write("*ERROR: ", argv[f], "expects argument!\n");
      os.exit(1);
    end;
  elseif arg[f] == "-h" or arg[f] == "-help" or arg[f] == "--help" then printHelp(); os.exit(1);
  elseif arg[f] == "--in-fake-root-mode-internal-option--" then inFakeRoot = true;
  elseif arg[f]:match("^-j") then
    local jn = tonumber(arg[f]:match("^-j(%d+)"));
    if not jn or jn < 1 then jn = 1; end;
    jobs = jn;
  else
    if apkgname then
      if bldnum then
        io.stderr:write("*ERROR: extra options!\n");
        os.exit(1);
      end;
      bldnum = tonumber(arg[f]);
      if not bldnum or bldnum < 1 then
        io.stderr:write("*ERROR: invalid build number!\n");
        os.exit(1);
      end;
      bldnum = math.floor(bldnum);
    else apkgname = arg[f];
    end;
  end;
end;


if not apkgname then
  print("*ERROR: no package name!");
  printHelp();
  os.exit(1);
end;


if force_native_profile then
  -- easy hack
  --defCompOptProfs.empty = defCompOptProfs.native;
  -- this, 'cause otherwise some libs (notably, OpenAL) doesn't work with DMD
  --defCompOptProfs.empty = defCompOptProfs.i586;
  --defCompOptProfs.empty = defCompOptProfs.haswell;
  --defCompOptProfs.empty = defCompOptProfs.nehalem;
  defCompOptProfs.empty = defCompOptProfs.core2;
  --defCompOptProfs.empty = defCompOptProfs.native;
end;


pinfo = parsePkgName(apkgname);
pinfo.build = bldnum or pinfo.build;
pkgdir = "/tmp/0sb_"..(pinfo.name);--..(os.time());


logMessage("building %s...", pkgInfoToStr(pinfo));

loadOptions(pinfo.name, true);
loadOptions("_dosb");
loadOptions("_local");

if ooNoPCP then myOpt.no_pcp = true; end;
if ooNoPRCP then myOpt.no_prcp = true; end;

if inst_pfx then
  print("FORCING PREFIX TO "..inst_pfx);
  myOpt.install_path = inst_pfx;
end;

--[[
for _, ppp in ipairs(postcfgPatches) do
  print("patch #".._..":");
  for _, s in ipairs(ppp) do print("  "..s.."|"); end;
end;
os.exit(1);
]]

-- last minute fixups
if not myOpt.chownUser or myOpt.chownUser == "" then
  olderr("too bad! we are mad! (no chown_user)");
end;

if not myOpt.srcBuildUser or myOpt.srcBuildUser == "" then
  local uname = myOpt.chownUser:match("^(.-):");
  if not uname then uname = myOpt.chownUser; end;
  if not uname or uname == "" then olderr("too bad! we are mad!"); end;
  myOpt.srcBuildUser = uname;
end;

--local pkgDestDir = "/home/ketmar/zoft/pkg/";
if not myOpt.pkgdestdir or myOpt.pkgdestdir == "" then
  myOpt.pkgdestdir = os.getenv("HOME").."/.sbuild/pkg";
end;
pkgDestDir = myOpt.pkgdestdir;


if ooNoCfg then myOpt.no_configure = true; end;
if ooNoPreCfg then myOpt.no_preconfigure = true; end;
if ooNoPreMk then myOpt.no_premake = true; end;
if ooNoMake then myOpt.no_make = true; end;
if ooNoPkg then myOpt.no_package = true; end;

local a = myOpt.pkg_author;
if a and a ~= "" then pinfo.author = a; end;

if doDump then
  dumpOptions();
end;

if dummy then
  os.exit(1);
end;


if myOpt.use_fakeroot then
  myOpt.srcBuildUser = "";
end;

--if not inFakeRoot and getUID() ~= 0 then
if getUID() ~= 0 then
  if myOpt.use_fakeroot then
    io.stderr:write("restarting in 'fakeroot' mode...\n");
    --local cmdline = "fakeroot "..(arg[0]):shQuote().." --in-fake-root-mode-internal-option--";
    local cmdline = "fakeroot "..(arg[0]):shQuote();
    for f = 1, #arg do
      cmdline = cmdline.." "..(arg[f]):shQuote();
    end;
    --io.stderr:write("[", cmdline, "]\n");
    local res = os.execute(cmdline);
    os.exit(res);
  else
    io.stderr:write("this script must be run with su/sudo!\n");
    io.stderr:flush();
    os.exit(2);
  end;
end;


if ooKBD == true then
  myOpt.keep_build_dir = true;
end;

buildDirName = "_sbuild_"..(pinfo.name);--..(os.time());
if myOpt.make_build_dir then
  os.execute("rm -rf "..buildDirName.." >/dev/null 2>&1");
  logMessage("making directory: %s", buildDirName);
  --print("making directory: "..buildDirName);
  os.execute("mkdir "..buildDirName.." >/dev/null 2>&1");
end;


local function sanitizePerms ()
  -- Make sure ownerships and permissions are sane
  --runCmd("chown -R root:root .");
  if myOpt.srcBuildUser ~= "" then
    logMessage("chown to "..myOpt.srcBuildUser);
    os.execute("chown "..(myOpt["srcBuildUser"]):shQuote().." -R .");
  end;
-- k8:  runCmd("find . -perm 666 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 664 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 600 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 444 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 400 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 440 -exec chmod 644 {} \\;");
-- k8:  runCmd("find . -perm 777 -exec chmod 755 {} \\;");
-- k8:  runCmd("find . -perm 775 -exec chmod 755 {} \\;");
-- k8:  runCmd("find . -perm 511 -exec chmod 755 {} \\;");
-- k8:  runCmd("find . -perm 711 -exec chmod 755 {} \\;");
-- k8:  runCmd("find . -perm 555 -exec chmod 755 {} \\;");
end;


local function runScriptList (sclist, msg)
  if #sclist > 0 then
    logMessage("executing custom '%s' script(s)...", msg);
    for _, script in ipairs(sclist) do
      runShCmdRoot(script, false);
    end;
    return true; -- something was executed
  end;
  return false; -- e4xecute 'standard'
end;


local function runConfigureStd ()
  if myOpt.no_configure then return; end;
  logMessage("configure...");
  local s = myOpt.cfgname;

  local ipath = myOpt.install_path or "/usr";
  ipath = ipath:match("^%s*(.-)%s*$");
  if ipath:match("/$") then ipath = ipath:sub(1, -2); end;
  if ipath == "" then ipath = "/usr"; end;

  if not myOpt.no_prefix then s = s.." --prefix="..ipath; end;
  if not myOpt.no_mandir then s = s.." --mandir="..ipath.."/man"; end;
  if not myOpt.no_docdir then s = s.." --docdir="..ipath.."/doc/"..pinfo.name.."-"..pinfo.version; end;
  if not myOpt.no_libdir then s = s.." --libdir="..ipath.."/lib"; end;
  --if not myOpt.no_localstatedir then s = s.." --localstatedir="..ipath.."/var"; end;
  local lsd = myOpt.localstatedir and myOpt.localstatedir or "/var";

  local scpath = "/etc";
  if ipath ~= "/usr" then
    myOpt.no_sysconfdir = true;
  end;
  if not myOpt.no_sysconfdir then s = s.." --sysconfdir="..scpath; end;
  if not myOpt.no_localstatedir then s = s.." --localstatedir="..lsd; end;

  if not myOpt.no_programprefix then s = s.." --program-prefix="; end;
  if not myOpt.no_programsuffix then s = s.." --program-suffix="; end;
  if not myOpt.no_buildarch then s = s.." --build="..(pinfo.arch).."-slackware-linux "; end;
  for _, opt in ipairs(confOpt) do
    --print("!", opt);
    s = s.." "..opt:shQuote();
  end;
  if not s:match("dependency%-tracking") and not myOpt.no_deptrack then
    s = s.." --disable-dependency-tracking";
  end;
  --logMessage("**RUN**: %s", s);
  runCmd(s);
end;


local function runConfigure ()
  if not myOpt.no_configure then
    if not runScriptList(scconfigurelist, "configure") then runConfigureStd(); end;
  end;
end;


local function runPreConfigure ()
  if not myOpt.no_preconfigure then
    runScriptList(scpreconfigurelist, "preconfigure");
  end;
end;


local function runPreMake ()
  if not myOpt.no_preconfigure then
    runScriptList(scpremakelist, "premake");
  end;
end;


local function runMakeStd ()
  if myOpt.no_make then return; end;
  logMessage("make...");
  local mk = myOpt.makename or "make";
  --mk = mk:shQuote();
  if jobs > 1 then
    if mk == "rake" then mk = mk.." -m"; end;
    mk = string.format("%s -j%i", mk, jobs);
  end;
  if not myOpt.no_make_argflags then mk = mk..getBuildFlags(true); end;
  for k, v in pairs(buildEnvVars) do if v ~= "" then mk = mk.." "..k:shQuote().."="..v:shQuote(); end; end;
  runCmd(mk);
end;


local function runMake ()
  if not myOpt.no_make then
    if not runScriptList(scmakelist, "make") then runMakeStd(); end;
  end;
end;


local function runInstallStd ()
  if myOpt.no_package then return; end;
  logMessage("install...");
  runCmdRoot("rm -fR "..pkgdir);
  local cmd;
  if myOpt.instname then cmd = myOpt.instname;
  elseif myOpt.makename then cmd = myOpt.makename.." install";
  else cmd = "make install";
  end;
  local v = myOpt.make_install_var or "DESTDIR";
  local pfx = "";
  local sfx = "";
  for k, v in pairs(buildEnvVars) do if v ~= "" then pfx = pfx.."export "..k.."="..(v:shQuote()).." ; "; end; end;
  if not myOpt.no_inst_destdir then
    if not myOpt.no_make_argflags then
      sfx = v.."="..(pkgdir:shQuote())..getBuildFlags(true);
    else
      pfx = pfx.."export "..v.."="..(pkgdir:shQuote()).." ; "..getBuildFlags();
    end;
  end;
  runCmdRoot(pfx.." "..cmd.." "..sfx);
end;


local function runInstall ()
  if not myOpt.no_package then
    if not runScriptList(scinstalllist, "install") then runInstallStd(); end;
  end;
end;


local function runStrip ()
  if not myOpt.no_strip then
    logMessage("stripping...");
    local s = [[
cd $PKG
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
]];
    --s = s:gsub("%$PKG", pkgdir);
    runShCmdRoot(s, true);
  end;
end;


local function compressMans ()
  logMessage("compressing mans...");
  runShCmdRoot([[
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.*
      )
    done
  )
fi
]], true);
end;


local function compressInfos ()
  logMessage("compressing infos...");
  runShCmdRoot([[
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi
if [ -d $PKG/usr/share/info ]; then
  ( cd $PKG/usr/share/info
    rm -f dir
    gzip -9 *
  )
fi
]], true);
end;


local function writeSDesc ()
  logMessage("writing slack-desc...");
  writeSlackDesc(pkgdir.."/install/");
end;


local function writeSReq ()
  logMessage("writing slack-required...");
  writeSlackReq(pkgdir.."/install/");
end;


local function copyDox ()
  if not myOpt.no_dox and #doclist > 0 then
    logMessage("copying dox...");
    for _, dinfo in ipairs(doclist) do
      if #dinfo.list > 0 then
        local path = pkgdir..getPrefixDir().."/doc/"..pinfo.name.."-"..pinfo.version;
        if dinfo.path ~= "" then
          if not dinfo.path:match("^/") then path = path.."/"; end;
          path = path..dinfo.path;
        end;
        local cpcmd = "cp -a";
        for _, msk in ipairs(dinfo.list) do cpcmd = cpcmd.." "..msk; end;
        if not cpcmd:match("^cp%s+%-a%s*$") then
          cpcmd = cpcmd.." "..path.."/";
          --print(("%s|").format(path));
          --print(cpcmd.."|");
          os.execute("mkdir -p "..path:shQuote());
          runCmdRootFuckFail(cpcmd);
        end;
      end;
    end;
  end;
end;


local function doPostInstall ()
  if not myOpt.no_pss and #psslist > 0 then
    logMessage("executing post-install scripts...");
    for _, script in ipairs(psslist) do
      runShCmdRoot(script, false);
    end;
  end;
end;


local function processConfigs ()
  if not myOpt.no_cfg and #cfglist > 0 then
    logMessage("process configs...");
    --
    local cl = {};
    for _, fname in ipairs(cfglist) do
      if fname:match("^/") then fname = fname:match("^/*(.*)$"); end;
      if fname ~= "" then
        local fl = io.open(pkgdir.."/"..fname, "r");
        if fl then
          fl:close();
          os.rename(pkgdir.."/"..fname, pkgdir.."/"..fname..".new");
          cl[#cl+1] = fname..".new";
        end;
      end;
    end;
    --
    if #cl > 0 then
      logMessage("making install script...");
      local dis = [[
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}
]];
      --'
      --config etc/acpi/acpi_handler.sh.new
      for _, fn in ipairs(cl) do dis = dis.."config "..fn.."\n"; end;
      doinstlist[#doinstlist+1] = dis;
    end;
  end;
end;


local function processDoInst ()
  if #doinstlist > 0 then
    logMessage("writing doist.sh...");
    local path = pkgdir.."/install/";
    os.execute("mkdir -p "..path:shQuote());
    local fl = io.open(path.."doinst.sh", "w");
    if not fl then error("can't create 'doinst.sh'"); end;
    for _, s in ipairs(doinstlist) do fl:write(s); end;
    fl:close();
  end;
end;


local function buildPkg ()
  logMessage("building package...");
  local xPkgExt = myOpt.pkgExt;
  if not xPkgExt or xPkgExt == "" then xPkgExt = "tgz"; end;
  local s = [[
cd $PKG
rm -f ../$PNAME.]]..xPkgExt..[[ 2>/dev/null
rm -f $DESTDIR$PNAME.]]..xPkgExt..[[ 2>/dev/null
/sbin/makepkg -l y -c y $DESTDIR$PNAME.]]..xPkgExt..[[
]];
  --s = s:gsub("%$PKG", pkgdir);
  s = s:gsub("%$PNAME", pkgInfoToStr(pinfo));
  s = s:gsub("%$DESTDIR", pkgDestDir);
  runShCmdRoot(s);
  local s = [[
cd $PKG
cd ..
rm -fR $PKG
]];
  --s = s:gsub("%$PKG", pkgdir);
  runShCmdRoot(s);
  if myOpt["chownUser"] ~= "" then
    local s = pkgDestDir..pkgInfoToStr(pinfo).."."..xPkgExt;
    os.execute("chown "..(myOpt["chownUser"]):shQuote().." "..s:shQuote());
  end;
end;


local function runPatch (lines)
  if #lines < 1 then return; end;
  local n = io.popen("patch -p1 --verbose", "w");
  for _, s in ipairs(lines) do n:write(s, "\n"); end;
  n:close();
end;


local function runPreConfigPatches ()
  if not myOpt.no_prcp then
    for _, ppp in ipairs(precfgPatches) do runPatch(ppp); end;
  end;
end;


local function runPostConfigPatches ()
  if not myOpt.no_pcp then
    for _, ppp in ipairs(postcfgPatches) do runPatch(ppp); end;
  end;
end;


--copyDox();
--os.exit(1);

fixFuckinCaret();
--copyEnvVars();
build_stage = "configure";
sanitizePerms();
runPreConfigPatches();
runPreConfigure();
runConfigure();
runPostConfigPatches();
build_stage = "make";
runPreMake();
runMake();
if not myOpt.no_package then
  io.write("MAKE COMPLETE!\n"); io.flush();
  os.execute("xcowsay 'build complete'");
  os.execute("sleep 2");

--[[
  if getUID() ~= 0 then
    --io.stderr:write("this script must be run with su/sudo!\n");
    --io.stderr:flush();
    --os.exit(2);
    local msg = "package "..table.concat(pinfo, "-").." compiled\nneed root provs to build it!";
    os.system(string.format("xcowsay %s", msg:shQuote()));
    print("executing sudo!");
    local cmd = 
    print(cmd);
    os.execute(string.format("sudo %s", cmd:shQuote());
  end;
]]

  runInstall();
  runStrip();
  compressMans();
  compressInfos();
  writeSDesc();
  writeSReq();
  copyDox();
  doPostInstall();
  processConfigs();
  processDoInst();
  buildPkg();
end;


if myOpt["chownUser"] ~= "" then
  logMessage("restoring owner: %s", myOpt["chownUser"]);
  os.execute("chown "..(myOpt["chownUser"]):shQuote().." -R .");
end;

if myOpt.make_build_dir and not myOpt.keep_build_dir then
  logMessage("removing directory: %s", buildDirName);
  os.execute("rm -rf "..buildDirName.." >/dev/null 2>&1");
end;

--runXCowSay("package building complete");
logMessage("done.");
